import { Fragment, useState, useEffect, useContext } from 'react'
import { Navigate } from 'react-router-dom'
import { Form, Button  } from 'react-bootstrap'
// import { } from 'react'
import Swal from 'sweetalert2'
import UserContext from './../UserContext'

export default function Login(){

const { user, setUser } = useContext(UserContext)
// console.log(user)

const [email, setEmail] = useState('')
const [password, setPassword] = useState('')
const [isActive, setIsActive] = useState(false)

function loginUser(e){
	e.preventDefault()

	// Syntax:
		// fetch(url, {options})
		// .then(res => res.json())
		// .then(data => {})``

	// console.log('hi')

	fetch('http://localhost:4000/api/users/login',
	{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}
	)
	.then(res => res.json())
	.then(data => {
		console.log(data)

		if(typeof data.access !== 'undefned'){

			localStorage.setItem('token', data.access)
			retrieveUserDetails(data.access)

			Swal.fire({
				title: 'Login Successful',
				icon: 'success',
				text: 'Welcome to Zuitt!'
			})
		} else {

			Swal.fire({
				title: 'Authentication Failed',
				icon: 'error',
				text: 'Check details and try again'
			})
		}
	})

	// localStorage.setItem('email', email)

	// setUser({
	// 	email: localStorage.getItem('email')
	// })
	// Clear input fields

	setEmail('')
	setPassword('')

	// alert(`${email} has been verified! Welcome back!`)
}

const retrieveUserDetails = (token) => {

	fetch('http://localhost:4000/api/users/details', {
		method: "POST",
		headers: {
			Authorization: `Bearer ${token}`
		}

	})
	.then(res => res.json())
	.then(data => {

		console.log(data)

		setUser({
			id: data._id,
			isAdmin: data.isAdmin
		})
	})
}

useEffect(() => 
{	if(email !== '' && password !== ''){
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [email, password])

	return (
	(user.id !== null) ?
	
		<Navigate to="/course"/>

		:


		<Fragment>
		<h1>Login</h1>
		<Form onSubmit={(e) => loginUser(e)}>
			<Form.Group controlId="loginEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder= "Enter your password here"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>


			{ isActive ?
				<Button variant="success" type="submit" id="loginBtn" className="mt-3">
							Submit
				</Button>

				:

				<Button variant="success" type="submit" id="loginBtn" className="mt-3" disabled>
							Submit
				</Button>
			}



		</Form>
		</Fragment>

	)
}