import { Fragment } from 'react'
import Banner from '../components/Banner.js'
import Highlights from '../components/Highlights.js'
// import Courses from '../components/Courses.js'

export default function Home(){
	return(
			<Fragment>
				<Banner/>
				<Highlights/>
				{/*<Courses/>*/}
				
			</Fragment>
		)
}