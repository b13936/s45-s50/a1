import { Link } from 'react-router-dom'
import { Row, Col, Button} from 'react-bootstrap'
// import Home from './Home.js'

export default function Error() {

	
	return(
			<Row>
				<Col>
					<h1>Page Not Found</h1>
					<p>
						Go back to the <a href='/' as={ Link } to="/">homepage.</a>
					</p>
{/*					<Button variant="primary mb-3">
						Enroll Now
					</Button>*/}
				</Col>
			</Row>
		)
}