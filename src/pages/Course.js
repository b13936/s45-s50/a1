import { Fragment, useEffect, useState } from 'react'
// import coursesData from '../data/coursesData'
import Courses from '../components/Courses'


export default function Course(){

	const [courses, setCourses] = useState([]);



	useEffect(() => {
		fetch('http://localhost:4000/api/courses')
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			setCourses(data.map(course => {
				return (
					<Courses key = {course._id} courseProp = {course}/>
				)
			}));
		});
	}, []);


	// console.log(coursesData)
	// console.log(coursesData[0])

	return (

		<Fragment>
			<h1>Courses</h1>
			{courses}
		</Fragment>

	)
}