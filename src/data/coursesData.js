const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus id saepe vitae natus, facere, quo quod repudiandae culpa, vel dolorum consectetur, quas hic? Et iste aperiam ad laboriosam dolorem! Iusto?",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus id saepe vitae natus, facere, quo quod repudiandae culpa, vel dolorum consectetur, quas hic? Et iste aperiam ad laboriosam dolorem! Iusto?",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Spring boot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus id saepe vitae natus, facere, quo quod repudiandae culpa, vel dolorum consectetur, quas hic? Et iste aperiam ad laboriosam dolorem! Iusto?",
		price: 55000,
		onOffer: true
	},


]

export default coursesData; 