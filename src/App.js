import { useState, Fragment, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom'
import { Routes, Route } from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
// import Banner from './components/Banner'
// import Highlights from '.components/Highlights'
import Course from './pages/Course'
import CourseView from './pages/CourseView'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import './App.css';
import { UserProvider } from './UserContext'

function App() {

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    // console.log(user)
    // console.log(localStorage)

    fetch('http://localhost:4000/api/users/details',
      {
          method: "POST",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)

        if(typeof data._id !=="undefined"){

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
      })
  }, [])


  // const [url, setUrl] = useState(Course)

  return (


    <UserProvider value= {{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
      <Container>
        <Routes>
          <Route exact path="/" element={<Home/>}/>
          <Route exact path="/course" element={<Course/>}/>
          <Route exact path ="/courses/:courseId" element={<CourseView/>}/>
          <Route exact path="/register" element={<Register/>}/>
          <Route exact path="/login" element={<Login/>}/>
          <Route exact path="/logout" element={<Logout/>}/>
          <Route path="*" element={<Error/>}/>
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
