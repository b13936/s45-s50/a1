import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'
// import AppNavbar from './components/AppNavbar'

// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

ReactDOM.render(
  <React.StrictMode>
    <App/>
    {/*<AppNavbar/>*/}
  </React.StrictMode>  ,
  document.getElementById('root')

);

/*const name = 'Peter Parker'


const user = {
  firstName: 'Tony',
  lastName: 'Stark'
}

const formatName = (user) => {
  return `${user.firstName} ${user.lastName}`
}


const element = <h1>Hello, {formatName(user)}</h1>*/
