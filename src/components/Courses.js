import { useState, useEffect } from 'react'
import { Row, Col, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Courses({courseProp}) {

	// console.log(props)
	// console.log(typeof props)
	console.log(courseProp)

	// courseProp deconstruct
	const {name, description, price, _id} = courseProp

	// enroll function
/*	function enroll(){
		setCount(count + 1)
		setSeat(seatCount - 1)
		console.log('Enrollees' + '' + count)
		console.log('Seats' + '' + seatCount)

		if(seatCount == 0){
			alert('no more seats');
			setCount(30)
			setSeat(0)
		}
	}*/
	// Syntax
		// const [getter, setter] = useState(initialValueofGetter)
	// const [count, setCount] = useState(0)
	// const [seats, setSeat] = useState(30)

	// function enroll(){

	// 	if(seats > 0){
	// 	setCount(count + 1)
	// 	setSeat(seats - 1)
	// 	console.log('seatsEnrollees' + ' ' + count)
	// 	console.log('Seats' + ' ' + seats)
	// 	}/* else {
	// 		alert('no more seats');
	// 	}*/

	// }

	// useEffect(() => {
	// 	if(seats === 0){
	// 		alert('No more seats available')
	// 	}
	// },[seats])


/*	function seats(){
	}*/


	return(
		<Card className= "mt-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn btn-primary" to={`/courses/${_id}`}>
					Details
				</Link>
			</Card.Body>
		</Card>	
	)
}