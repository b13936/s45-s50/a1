import { Row, Col, Card} from 'react-bootstrap'

export default function Highlights() {
	return(
		<Row className="HL">
			<Col xs={12} md={3}>
				<Card className= "mt-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus eligendi debitis veniam obcaecati, rerum laudantium sint assumenda, nisi, itaque recusandae quasi ipsa harum possimus rem. Blanditiis, corporis, pariatur. Qui, incidunt?
						</Card.Text>
					</Card.Body>
				</Card>	
			</Col>	

			<Col xs={12} md={3}>
				<Card className= "mt-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus eligendi debitis veniam obcaecati, rerum laudantium sint assumenda, nisi, itaque recusandae quasi ipsa harum possimus rem. Blanditiis, corporis, pariatur. Qui, incidunt?
						</Card.Text>
					</Card.Body>
				</Card>	
			</Col>	

			<Col xs={12} md={3}>
				<Card className= "mt-3">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of our Community</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus eligendi debitis veniam obcaecati, rerum laudantium sint assumenda, nisi, itaque recusandae quasi ipsa harum possimus rem. Blanditiis, corporis, pariatur. Qui, incidunt?
						</Card.Text>
					</Card.Body>
				</Card>	
			</Col>	
		</Row>
	)
}